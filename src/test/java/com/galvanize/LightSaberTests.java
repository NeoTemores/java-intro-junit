package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LightSaberTests {
    LightSaber lightSaber;
    @BeforeEach
    public void setUp(){
        lightSaber = new LightSaber(1337);
    }
    @Test
    public void testSetGetCharge(){
        lightSaber.setCharge(50.0f);
        assertEquals(50.0f, lightSaber.getCharge());
    }
    @Test
    public void testSetGetColor(){
        lightSaber.setColor("purple");
        assertEquals("purple", lightSaber.getColor() );
    }
    @Test
    public void testGetJediSerialNum(){
        assertEquals(1337, lightSaber.getJediSerialNumber());
    }
    @Test
    public void testUseGetCharge(){
        lightSaber.use(10.0f);
        assertEquals(98.333336f,lightSaber.getCharge());
    }
    @Test
    public void testGetCharge(){
        assertEquals(100f, lightSaber.getCharge());
    }
    @Test
    public void testGetRemainingMin(){
        assertEquals(300f, lightSaber.getRemainingMinutes());
    }
    @Test
    public void testRecharge(){
        lightSaber.setCharge(10f);
        lightSaber.recharge();
        assertEquals(100f, lightSaber.getCharge());
    }
}
